﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ZVI_4_Threshold
{
    public partial class Histogram : Form
    {
        private bool filtered;
        private int[,] histogramData;
        const int border = 10;

        public Histogram()
        {
            InitializeComponent();
            int w = 256 + 2 * border;
            int h = 200 + 2 * border;
            Width = w - ClientSize.Width + Size.Width;
            Height = h - ClientSize.Height + Size.Height;

            this.histogramData = new int[256, 3];
        }

        public Histogram(ImageData imageData, bool filtered)
        {
            this.filtered = filtered;
            this.histogramData = imageData.ComputeHistogram();
            if (filtered)
                ImageData.FilterHistogram(this.histogramData);

            InitializeComponent();
            int w = 256 + 2 * border;
            int h = 200 + 2 * border;
            Width = w - ClientSize.Width + Size.Width;
            Height = h - ClientSize.Height + Size.Height;
        }

        protected override void  OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            Graphics g = e.Graphics;
            
            int ymax = 0;
            for (int i = 0; i < 256; i++)
            {
                for (int j = 0; j < 3; j++)
                    if (histogramData[i, j] > ymax)
                        ymax = histogramData[i, j];
            }

            float ymult = (float)(ClientSize.Height - 2 * border) / ymax;
            g.FillRectangle(Brushes.Black, border, border, 256, ClientSize.Height - 2 * border + 1);

            for (int i = 0; i < 256; i++)
            {
                int maxval = 0;
                int maxj = 0;
                for (int j = 0; j < 3; j++)
                {
                    if (histogramData[i, j] > maxval)
                    {
                        maxval = histogramData[i, j];
                        maxj = j;
                    }
                }

                int x = i + border;
                int y = ClientSize.Height - border;
                switch (maxj)
                {
                    case 0:
                        g.DrawLine(Pens.Red, x, y - ymult * histogramData[i, 0], x, y);
                        if (histogramData[i, 1] > histogramData[i, 2])
                        {
                            g.DrawLine(Pens.Yellow, x, y - ymult * histogramData[i, 1], x, y);
                            g.DrawLine(Pens.White, x, y - ymult * histogramData[i, 2], x, y);
                        }
                        else
                        {
                            g.DrawLine(Pens.Fuchsia, x, y - ymult * histogramData[i, 2], x, y);
                            g.DrawLine(Pens.White, x, y - ymult * histogramData[i, 1], x, y);
                        }
                        break;
                    case 1:
                        g.DrawLine(Pens.Lime, x, y - ymult * histogramData[i, 1], x, y);
                        if (histogramData[i, 0] > histogramData[i, 2])
                        {
                            g.DrawLine(Pens.Yellow, x, y - ymult * histogramData[i, 0], x, y);
                            g.DrawLine(Pens.White, x, y - ymult * histogramData[i, 2], x, y);
                        }
                        else
                        {
                            g.DrawLine(Pens.Aqua, x, y - ymult * histogramData[i, 2], x, y);
                            g.DrawLine(Pens.White, x, y - ymult * histogramData[i, 0], x, y);
                        }
                        break;
                    case 2:
                        g.DrawLine(Pens.Blue, x, y - ymult * histogramData[i, 2], x, y);
                        if (histogramData[i, 0] > histogramData[i, 1])
                        {
                            g.DrawLine(Pens.Fuchsia, x, y - ymult * histogramData[i, 0], x, y);
                            g.DrawLine(Pens.White, x, y - ymult * histogramData[i, 1], x, y);
                        }
                        else
                        {
                            g.DrawLine(Pens.Aqua, x, y - ymult * histogramData[i, 1], x, y);
                            g.DrawLine(Pens.White, x, y - ymult * histogramData[i, 0], x, y);
                        }
                        break;
                }
            }
        }
    }
}
