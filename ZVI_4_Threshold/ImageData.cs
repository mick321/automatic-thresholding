﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace ZVI_4_Threshold
{
    public enum GrayScaleConversion
    {
        Average,
        Luminosity,
        Red,
        Green,
        Blue
    }

    /// <summary>
    /// Decoded image data in raw form.
    /// </summary>
    public class ImageData
    {
        private int width, height, channels;    // channel count (1 = grayscale, 3 = RGB)
        private byte[ , , ] pixels;             // pixels (indices x, y, channel)

        /// <summary>
        /// Construct empty image.
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="grayscale"></param>
        public ImageData(int width, int height, bool grayscale)
        {
            this.width = width;
            this.height = height;
            channels = grayscale ? 1 : 3;
            pixels = new byte[width, height, channels];
        }

        /// <summary>
        /// Construct from bitmap.
        /// </summary>
        /// <param name="bitmap">bitmap reference</param>
        /// <param name="grayscale">work only with grayscale</param>
        public ImageData(Bitmap bitmap, bool grayscale)
        {
            width = bitmap.Width;
            height = bitmap.Height;
            channels = grayscale ? 1 : 3;
            pixels = new byte[width, height, channels];

            BitmapData sourceData = bitmap.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            
            byte[] sourcePixels = new byte[Math.Abs(sourceData.Stride) * height];
            int stride = sourceData.Stride;
            System.Runtime.InteropServices.Marshal.Copy(sourceData.Scan0, sourcePixels, 0, Math.Abs(sourceData.Stride) * height);

            bitmap.UnlockBits(sourceData);

            int i = 0;
            if (channels == 3)
            {
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        pixels[x, y, 0] = sourcePixels[i + 2];
                        pixels[x, y, 1] = sourcePixels[i + 1];
                        pixels[x, y, 2] = sourcePixels[i];
                        i += 4;
                    }
                }
            }
            else
            {
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        pixels[x, y, 0] = sourcePixels[i];
                        i += 4;
                    }
                }
            }
        }

        /// <summary>
        /// Get bitmap with these image data.
        /// </summary>
        /// <returns></returns>
        public Bitmap GetDataAsBitmap()
        {
            byte[] sourcePixels = new byte[width * height * 4];
            int i = 0;
            if (channels == 3)
            {
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        sourcePixels[i + 2] = pixels[x, y, 0];
                        sourcePixels[i + 1] = pixels[x, y, 1];
                        sourcePixels[i] = pixels[x, y, 2];
                        sourcePixels[i + 3] = 255;
                        i += 4;
                    }
                }
            }
            else
            {
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        sourcePixels[i + 2] = pixels[x, y, 0];
                        sourcePixels[i + 1] = pixels[x, y, 0];
                        sourcePixels[i] = pixels[x, y, 0];
                        sourcePixels[i + 3] = 255;
                        i += 4;
                    }
                }
            }

            int stride = width * 4;
            GCHandle pinnedArray = GCHandle.Alloc(sourcePixels, GCHandleType.Pinned);
            IntPtr pointer = pinnedArray.AddrOfPinnedObject();
            Bitmap bmp = new Bitmap(width, height, stride, PixelFormat.Format32bppArgb, pointer);
            pinnedArray.Free();
            return bmp;
        }

        /// <summary>
        /// Returns true when working only with one channel.
        /// </summary>
        /// <returns></returns>
        public bool IsGrayscale()
        {
            return channels == 1;
        }

        /// <summary>
        /// Computes histogram.
        /// </summary>
        /// <returns>Array[brightness, channel]</returns>
        public int[,] ComputeHistogram()
        {
            int[,] histogram = new int[256, 3];
            if (channels == 3)
            {
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        histogram[pixels[x, y, 0], 0]++;
                        histogram[pixels[x, y, 1], 1]++;
                        histogram[pixels[x, y, 2], 2]++;
                    }
                }
            }
            else
            {
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        histogram[pixels[x, y, 0], 0]++;
                        histogram[pixels[x, y, 0], 1]++;
                        histogram[pixels[x, y, 0], 2]++;
                    }
                }
            }
            return histogram;
        }

        public static void FilterHistogram(int[,] histogram)
        {
            int channels = histogram.GetLength(1);
            int[,] temporaryHist = new int[256, channels];

            for (int i = 0; i < 256; i++)
                for (int c = 0; c < channels; c++)
                {
                    int sum = 0, count = 0;
                    for (int k = i - 3; k <= i + 3; k++)
                        if (k >= 0 && k < 256)
                        {
                            sum += histogram[k, c];
                            count++;
                        }
                    temporaryHist[i, c] = sum / count;
                }

            for (int i = 0; i < 256; i++)
                for (int c = 0; c < channels; c++)
                    histogram[i, c] = temporaryHist[i, c];
            
            for (int i = 0; i < 256; i++)
                for (int c = 0; c < channels; c++)
                {
                    int maximum = Int32.MinValue;
                    for (int k = i - 2; k <= i + 2; k++)
                        if (k >= 0 && k < 256)
                        {
                            maximum = Math.Max(maximum, histogram[k, c]);
                        }
                    temporaryHist[i, c] = maximum;
                }
            
            for (int i = 0; i < 256; i++)
                for (int c = 0; c < channels; c++)
                {
                    int minimum = Int32.MaxValue;
                    for (int k = i - 2; k <= i + 2; k++)
                        if (k >= 0 && k < 256)
                        {
                            minimum = Math.Min(minimum, temporaryHist[k, c]);
                        }
                    histogram[i, c] = minimum;
                }
        }

        public ImageData CreateGrayscaleImage(GrayScaleConversion method)
        {
            if (IsGrayscale())
                return this;

            ImageData imgData = new ImageData(width, height, true);

            if (method == GrayScaleConversion.Average)
            {
                for (int x = 0; x < width; x++)
                    for (int y = 0; y < height; y++)
                    {
                        imgData.pixels[x, y, 0] = (byte)(((int)pixels[x, y, 0] + (int)pixels[x, y, 1] + (int)pixels[x, y, 2]) / 3);
                    }
            }
            else if (method == GrayScaleConversion.Luminosity)
            {
                for (int x = 0; x < width; x++)
                    for (int y = 0; y < height; y++)
                    {
                        imgData.pixels[x, y, 0] = (byte)(Math.Min(255.0, 0.2125 * pixels[x, y, 0] + 0.7154 * pixels[x, y, 1] + 0.0721 * pixels[x, y, 2]));
                    }
            }
            else if (method == GrayScaleConversion.Red)
            {
                for (int x = 0; x < width; x++)
                    for (int y = 0; y < height; y++)
                    {
                        imgData.pixels[x, y, 0] = pixels[x, y, 0];
                    }
            }
            else if (method == GrayScaleConversion.Green)
            {
                for (int x = 0; x < width; x++)
                    for (int y = 0; y < height; y++)
                    {
                        imgData.pixels[x, y, 0] = pixels[x, y, 1];
                    }
            }
            else if (method == GrayScaleConversion.Blue)
            {
                for (int x = 0; x < width; x++)
                    for (int y = 0; y < height; y++)
                    {
                        imgData.pixels[x, y, 0] = pixels[x, y, 2];
                    }
            }

            return imgData;
        }

        public ImageData CreateThresholdedImage(int[] thresholds)
        {
            if (!IsGrayscale() || thresholds == null || thresholds.Length == 0)
                return null;

            ImageData imgData = new ImageData(width, height, true);
            for (int x = 0; x < width; x++)
                for (int y = 0; y < height; y++)
                {
                    int ti = 0;
                    for (int i = 0; i < thresholds.Length && pixels[x, y, 0] > thresholds[i]; i++)
                        ti = i + 1;
                    imgData.pixels[x, y, 0] = (byte)(255 * ti / thresholds.Length);
                }
            return imgData;
        }
    }
}
