﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ZVI_4_Threshold
{
    public partial class InputBox : Form
    {
        public int value;

        public InputBox()
        {
            InitializeComponent();
        }

        public InputBox(int initialValue, string caption)
        {
            InitializeComponent();
            numericUpDown.Value = initialValue;
            Text = caption;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            value = (int)numericUpDown.Value;
        }
    }
}
