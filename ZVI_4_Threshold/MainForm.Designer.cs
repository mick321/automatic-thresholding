﻿namespace ZVI_4_Threshold
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.histogramOfSourceRGBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convertToGrayscaleLuminosityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convertToGrayscaleAverageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convertToGrayscaleOnlyRedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convertToGrayscaleOnlyGreenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convertToGrayscaleOnlyBlueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.automaticThresholdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualThresholdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.dlgLoadPicture = new System.Windows.Forms.OpenFileDialog();
            this.dlgSavePicture = new System.Windows.Forms.SaveFileDialog();
            this.filteredHistogramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.imageToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(641, 24);
            this.mainMenu.TabIndex = 0;
            this.mainMenu.Text = "mainMenu";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openImageToolStripMenuItem,
            this.saveResultToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openImageToolStripMenuItem
            // 
            this.openImageToolStripMenuItem.Name = "openImageToolStripMenuItem";
            this.openImageToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openImageToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.openImageToolStripMenuItem.Text = "&Open image...";
            this.openImageToolStripMenuItem.Click += new System.EventHandler(this.openImageToolStripMenuItem_Click);
            // 
            // saveResultToolStripMenuItem
            // 
            this.saveResultToolStripMenuItem.Name = "saveResultToolStripMenuItem";
            this.saveResultToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveResultToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.saveResultToolStripMenuItem.Text = "&Save image...";
            this.saveResultToolStripMenuItem.Click += new System.EventHandler(this.saveResultToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(188, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.undoToolStripMenuItem.Text = "&Undo";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.redoToolStripMenuItem.Text = "&Redo";
            this.redoToolStripMenuItem.Click += new System.EventHandler(this.redoToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.histogramOfSourceRGBToolStripMenuItem,
            this.filteredHistogramToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "&View";
            // 
            // histogramOfSourceRGBToolStripMenuItem
            // 
            this.histogramOfSourceRGBToolStripMenuItem.Name = "histogramOfSourceRGBToolStripMenuItem";
            this.histogramOfSourceRGBToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.histogramOfSourceRGBToolStripMenuItem.Text = "&Histogram...";
            this.histogramOfSourceRGBToolStripMenuItem.Click += new System.EventHandler(this.histogramOfSourceRGBToolStripMenuItem_Click);
            // 
            // imageToolStripMenuItem
            // 
            this.imageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.convertToGrayscaleLuminosityToolStripMenuItem,
            this.convertToGrayscaleAverageToolStripMenuItem,
            this.convertToGrayscaleOnlyRedToolStripMenuItem,
            this.convertToGrayscaleOnlyGreenToolStripMenuItem,
            this.convertToGrayscaleOnlyBlueToolStripMenuItem,
            this.toolStripMenuItem2,
            this.automaticThresholdToolStripMenuItem,
            this.manualThresholdToolStripMenuItem});
            this.imageToolStripMenuItem.Name = "imageToolStripMenuItem";
            this.imageToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.imageToolStripMenuItem.Text = "&Image";
            // 
            // convertToGrayscaleLuminosityToolStripMenuItem
            // 
            this.convertToGrayscaleLuminosityToolStripMenuItem.Name = "convertToGrayscaleLuminosityToolStripMenuItem";
            this.convertToGrayscaleLuminosityToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this.convertToGrayscaleLuminosityToolStripMenuItem.Size = new System.Drawing.Size(311, 22);
            this.convertToGrayscaleLuminosityToolStripMenuItem.Text = "Convert to Grayscale (&Luminosity)";
            this.convertToGrayscaleLuminosityToolStripMenuItem.Click += new System.EventHandler(this.convertToGrayscaleLuminosityToolStripMenuItem_Click);
            // 
            // convertToGrayscaleAverageToolStripMenuItem
            // 
            this.convertToGrayscaleAverageToolStripMenuItem.Name = "convertToGrayscaleAverageToolStripMenuItem";
            this.convertToGrayscaleAverageToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.G)));
            this.convertToGrayscaleAverageToolStripMenuItem.Size = new System.Drawing.Size(311, 22);
            this.convertToGrayscaleAverageToolStripMenuItem.Text = "Convert to Grayscale (&Average)";
            this.convertToGrayscaleAverageToolStripMenuItem.Click += new System.EventHandler(this.convertToGrayscaleAverageToolStripMenuItem_Click);
            // 
            // convertToGrayscaleOnlyRedToolStripMenuItem
            // 
            this.convertToGrayscaleOnlyRedToolStripMenuItem.Name = "convertToGrayscaleOnlyRedToolStripMenuItem";
            this.convertToGrayscaleOnlyRedToolStripMenuItem.Size = new System.Drawing.Size(311, 22);
            this.convertToGrayscaleOnlyRedToolStripMenuItem.Text = "Convert to Grayscale (Only &Red)";
            this.convertToGrayscaleOnlyRedToolStripMenuItem.Click += new System.EventHandler(this.convertToGrayscaleOnlyRedToolStripMenuItem_Click);
            // 
            // convertToGrayscaleOnlyGreenToolStripMenuItem
            // 
            this.convertToGrayscaleOnlyGreenToolStripMenuItem.Name = "convertToGrayscaleOnlyGreenToolStripMenuItem";
            this.convertToGrayscaleOnlyGreenToolStripMenuItem.Size = new System.Drawing.Size(311, 22);
            this.convertToGrayscaleOnlyGreenToolStripMenuItem.Text = "Convert to Grayscale (Only &Green)";
            this.convertToGrayscaleOnlyGreenToolStripMenuItem.Click += new System.EventHandler(this.convertToGrayscaleOnlyGreenToolStripMenuItem_Click);
            // 
            // convertToGrayscaleOnlyBlueToolStripMenuItem
            // 
            this.convertToGrayscaleOnlyBlueToolStripMenuItem.Name = "convertToGrayscaleOnlyBlueToolStripMenuItem";
            this.convertToGrayscaleOnlyBlueToolStripMenuItem.Size = new System.Drawing.Size(311, 22);
            this.convertToGrayscaleOnlyBlueToolStripMenuItem.Text = "Convert to Grayscale (Only &Blue)";
            this.convertToGrayscaleOnlyBlueToolStripMenuItem.Click += new System.EventHandler(this.convertToGrayscaleOnlyBlueToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(308, 6);
            // 
            // automaticThresholdToolStripMenuItem
            // 
            this.automaticThresholdToolStripMenuItem.Name = "automaticThresholdToolStripMenuItem";
            this.automaticThresholdToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.automaticThresholdToolStripMenuItem.Size = new System.Drawing.Size(311, 22);
            this.automaticThresholdToolStripMenuItem.Text = "Automatic &Threshold...";
            this.automaticThresholdToolStripMenuItem.Click += new System.EventHandler(this.automaticThresholdToolStripMenuItem_Click);
            // 
            // manualThresholdToolStripMenuItem
            // 
            this.manualThresholdToolStripMenuItem.Name = "manualThresholdToolStripMenuItem";
            this.manualThresholdToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.T)));
            this.manualThresholdToolStripMenuItem.Size = new System.Drawing.Size(311, 22);
            this.manualThresholdToolStripMenuItem.Text = "&Manual Threshold...";
            this.manualThresholdToolStripMenuItem.Click += new System.EventHandler(this.manualThresholdToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBox.Location = new System.Drawing.Point(12, 27);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(617, 427);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 1;
            this.pictureBox.TabStop = false;
            // 
            // dlgLoadPicture
            // 
            this.dlgLoadPicture.Filter = "Image files|*.jpg;*.jpeg;*.png;*.bmp;*.gif";
            this.dlgLoadPicture.Title = "Load picture...";
            this.dlgLoadPicture.FileOk += new System.ComponentModel.CancelEventHandler(this.dlgLoadPicture_FileOk);
            // 
            // dlgSavePicture
            // 
            this.dlgSavePicture.Filter = "PNG format|*.png";
            this.dlgSavePicture.Title = "Save picture...";
            this.dlgSavePicture.FileOk += new System.ComponentModel.CancelEventHandler(this.dlgSavePicture_FileOk);
            // 
            // filteredHistogramToolStripMenuItem
            // 
            this.filteredHistogramToolStripMenuItem.Name = "filteredHistogramToolStripMenuItem";
            this.filteredHistogramToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.filteredHistogramToolStripMenuItem.Text = "Filtered Histogram...";
            this.filteredHistogramToolStripMenuItem.Click += new System.EventHandler(this.filteredHistogramToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 466);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.mainMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mainMenu;
            this.Name = "MainForm";
            this.Text = "Automatic Threshold (based on histogram shape)";
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem histogramOfSourceRGBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem convertToGrayscaleAverageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem convertToGrayscaleLuminosityToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem automaticThresholdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manualThresholdToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.OpenFileDialog dlgLoadPicture;
        private System.Windows.Forms.SaveFileDialog dlgSavePicture;
        private System.Windows.Forms.ToolStripMenuItem convertToGrayscaleOnlyRedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem convertToGrayscaleOnlyGreenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem convertToGrayscaleOnlyBlueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filteredHistogramToolStripMenuItem;
    }
}

