﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ZVI_4_Threshold
{
    /// <summary>
    /// Main Form. It displays image and offers actions above it.
    /// </summary>
    public partial class MainForm : Form
    {
        int autoThresholdMaxCount;
        Stack<ImageData> imageData;
        Stack<ImageData> imageDataRedoStack;
        /// <summary>
        /// Constructor.
        /// </summary>
        public MainForm()
        {
            autoThresholdMaxCount = 4;
            imageData = new Stack<ImageData>();
            imageDataRedoStack = new Stack<ImageData>();
            InitializeComponent();
        }

        // -----

        public void LoadImage(string filename)
        {
            try
            {
                pictureBox.Load(filename);
            }
            catch
            {
                MessageBox.Show("Loading picture failed.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            imageData.Clear();
            if (imageData.Count == 0)
                imageData.Push(new ImageData((Bitmap)pictureBox.Image, false));
            else
                imageData.Push(new ImageData((Bitmap)pictureBox.Image, imageData.Peek().IsGrayscale()));
        }

        public void SaveImage(string filename)
        {
            if (pictureBox.Image == null)
                return;
            pictureBox.Image.Save(dlgSavePicture.FileName);
        }

        public void HistoryPush(ImageData historyPoint)
        {
            if (imageData.Count > 0 && imageData.Peek() == historyPoint)
                return;

            imageData.Push(historyPoint);
            pictureBox.Image = historyPoint.GetDataAsBitmap();
            imageDataRedoStack.Clear();
        }

        public void HistoryUndo()
        {
            if (imageData.Count <= 1)
                return;

            imageDataRedoStack.Push(imageData.Pop());
            pictureBox.Image = imageData.Peek().GetDataAsBitmap();
        }

        public void HistoryRedo()
        {
            if (imageDataRedoStack.Count == 0)
                return;

            ImageData historyPoint = imageDataRedoStack.Pop();
            imageData.Push(historyPoint);
            pictureBox.Image = historyPoint.GetDataAsBitmap();
        }

        // -----

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox about = new AboutBox();
            about.ShowDialog(this);
        }

        private void dlgLoadPicture_FileOk(object sender, CancelEventArgs e)
        {
            LoadImage(dlgLoadPicture.FileName);
        }

        private void openImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dlgLoadPicture.ShowDialog();
        }

        private void saveResultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dlgSavePicture.ShowDialog();
        }

        private void dlgSavePicture_FileOk(object sender, CancelEventArgs e)
        {
            SaveImage(dlgSavePicture.FileName);
        }

        private void histogramOfSourceRGBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imageData.Count == 0)
                return;

            Histogram histogram = new Histogram(imageData.Peek(), false);
            histogram.ShowDialog(this);
        }

        private void filteredHistogramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imageData.Count == 0)
                return;

            Histogram histogram = new Histogram(imageData.Peek(), true);
            histogram.ShowDialog(this);
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HistoryUndo();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HistoryRedo();
        }

        private void convertToGrayscaleAverageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imageData.Count == 0)
                return;

            HistoryPush(imageData.Peek().CreateGrayscaleImage(GrayScaleConversion.Average));
        }

        private void convertToGrayscaleLuminosityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imageData.Count == 0)
                return;

            HistoryPush(imageData.Peek().CreateGrayscaleImage(GrayScaleConversion.Luminosity));
        }

        private void convertToGrayscaleOnlyRedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imageData.Count == 0)
                return;

            HistoryPush(imageData.Peek().CreateGrayscaleImage(GrayScaleConversion.Red));
        }

        private void convertToGrayscaleOnlyGreenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imageData.Count == 0)
                return;

            HistoryPush(imageData.Peek().CreateGrayscaleImage(GrayScaleConversion.Green));
        }

        private void convertToGrayscaleOnlyBlueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imageData.Count == 0)
                return;

            HistoryPush(imageData.Peek().CreateGrayscaleImage(GrayScaleConversion.Blue));
        }

        private void manualThresholdToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imageData.Count == 0)
                return;

            ImageData imgGrayScale;
            if (!imageData.Peek().IsGrayscale())
                imgGrayScale = imageData.Peek().CreateGrayscaleImage(GrayScaleConversion.Luminosity);
            else
                imgGrayScale = imageData.Peek();

            Threshold threshold = new Threshold(imgGrayScale);
            threshold.ShowDialog();
            int[] thresholds = threshold.GetThresholds();
            if (thresholds == null)
                return;

            ImageData imgResult = imgGrayScale.CreateThresholdedImage(thresholds);
            if (imgResult != null)
                HistoryPush(imgResult);
        }

        private void automaticThresholdToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imageData.Count == 0)
                return;

            ImageData imgGrayScale;
            if (!imageData.Peek().IsGrayscale())
                imgGrayScale = imageData.Peek().CreateGrayscaleImage(GrayScaleConversion.Luminosity);
            else
                imgGrayScale = imageData.Peek();

            InputBox ibox = new InputBox(autoThresholdMaxCount, "Enter maximum count of thresholds");
            if (ibox.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                autoThresholdMaxCount = ibox.value;
                int[,] histogram = imgGrayScale.ComputeHistogram();
                ImageData.FilterHistogram(histogram);

                List<int> thresholds = new List<int>(ibox.value);
                bool goingDown = false;
                for (int i = 1; i < 255; i++)
                {
                    if (histogram[i - 1, 0] > histogram[i, 0])
                        goingDown = true;

                    if (goingDown && histogram[i, 0] < histogram[i + 1, 0])
                    {
                        thresholds.Add(i);
                        goingDown = false;
                    } 
                }

                if (thresholds.Count == 0)
                    thresholds.Add(128);

                bool removeOnEnd = false;
                while (autoThresholdMaxCount < thresholds.Count)
                {
                    thresholds.RemoveAt(removeOnEnd ? thresholds.Count - 1 : 0);
                }

                thresholds.Sort();

                ImageData imgResult = imgGrayScale.CreateThresholdedImage(thresholds.ToArray());
                if (imgResult != null)
                    HistoryPush(imgResult);
            }
        }
    }
}
