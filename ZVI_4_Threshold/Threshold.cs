﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ZVI_4_Threshold
{
    public partial class Threshold : Form
    {
        private int[,] histogramData;
        const int border = 15;
        SortedSet<int> thresholds;

        public Threshold()
        {
            InitializeComponent();
            int w = 256 + 2 * border;
            int h = 200 + 6 * border;
            Width = w - ClientSize.Width + Size.Width;
            Height = h - ClientSize.Height + Size.Height;

            this.histogramData = new int[256,3];
            thresholds = new SortedSet<int>();
        }

        public Threshold(ImageData imageData)
        {
            this.histogramData = imageData.ComputeHistogram();
            InitializeComponent();
            int w = 256 + 2 * border;
            int h = 200 + 6 * border;
            Width = w - ClientSize.Width + Size.Width;
            Height = h - ClientSize.Height + Size.Height;
            thresholds = new SortedSet<int>();
        }

        protected override void  OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            Graphics g = e.Graphics;
            
            int ymax = 0;
            for (int i = 0; i < 256; i++)
            {
                for (int j = 0; j < 3; j++)
                    if (histogramData[i, j] > ymax)
                        ymax = histogramData[i, j];
            }

            float ymult = (float)(ClientSize.Height - 6 * border) / ymax;
            g.FillRectangle(Brushes.Black, border, border, 256, ClientSize.Height - 6 * border + 2);

            for (int i = 0; i < 256; i++)
            {
                int maxval = 0;
                int maxj = 0;
                for (int j = 0; j < 3; j++)
                {
                    if (histogramData[i, j] > maxval)
                    {
                        maxval = histogramData[i, j];
                        maxj = j;
                    }
                }

                int x = i + border;
                int y = ClientSize.Height - 5 * border;
                g.DrawLine(Pens.White, x, y - ymult * maxval, x, y);
            }

            foreach (int t in thresholds)
            {
              int y = ClientSize.Height - 5 * border;
              g.DrawLine(Pens.Red, border + t, y - (float)(ClientSize.Height - 6 * border), border + t, y);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            thresholds.Clear();
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Threshold_MouseClick(object sender, MouseEventArgs e)
        {
            int x = e.X - border;
            if (x > 0 && x < 255)
            {
                if (e.Button == MouseButtons.Left)
                {
                    thresholds.Add(x);
                    Invalidate();
                }
                else if (e.Button == MouseButtons.Right)
                {
                    int k = 0;
                    while (k < 10 && !thresholds.Remove(x - k) && !thresholds.Remove(x + k))
                        k++;

                    Invalidate();
                }
            }
        }

        public int[] GetThresholds()
        {
            if (thresholds.Count == 0)
                return null;

            return thresholds.ToArray();
        }
    }
}
